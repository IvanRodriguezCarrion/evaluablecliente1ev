var team = new Array("Fnatic","Gambit","Lemondogs","Cloud");
var fnatic = new Array("sOAZ","Cyanide","xPeke","Puszu","YellOwStaR");
var gambit = new Array("Darien","Diamond","Alex","Genja","Voidle");
var lemondogs = new Array("Zorozero","Dexter","Nukeduck","Tabzz","Sdf");
var cloud = new Array("Balls","Meteos","Hai","Sneaky","LemonNation");
            
function cTeam() { // rellana comboBox de equipos
    var teams = document.getElementById('equipos');
    for (var i=1; i<=team.length; i++) {
        teams.options[i]=new Option(team[i-1]);
    }
}
            
function cPlayers(id) { // a partir de la id del cTeam, rellena con el equipo adecuado.
    var cbPlayers = document.getElementById('jugadores');
    switch(id) {
        case 1: 
            for (var i=0; i<equipos.length; i++) {
                cbPlayers.options[i]=new Option(fnatic[i]);
            }
            break;
        case 2: 
            for (var i=0; i<equipos.length; i++) {
                cbPlayers.options[i]=new Option(gambit[i]);
            }
            break;
        case 3: 
            for (var i=0; i<equipos.length; i++) {
                cbPlayers.options[i]=new Option(lemondogs[i]);
            }
            break;
        case 4: 
            for (var i=0; i<equipos.length; i++) {
                cbPlayers.options[i]=new Option(cloud[i]);
                }
            break;
        default:
            cbPlayers.options.length = 0; // no se selecciona ningún equipo, se vacia.
            break;
        }
}

window.onload = function() { //rellenado del cb automático al cargar
    cTeam();
    document.getElementById("equipos").addEventListener("change", function(){cPlayers(this.selectedIndex)}, false);
}