//El método toString debería devolvernos el mismo valor si le pasamos el string y la base que nos pasan para ser correcto.

function boton () { // convierte num a base seleccionada
    var tbValor = document.getElementById('valor').value.toLowerCase();
    var tbBaseO = document.getElementById('baseOrigen').value;
    var tbBaseF = document.getElementById('baseFinal').value;
    
    if (parseInt(tbBaseO)<2 || parseInt(tbBaseO)>36 || parseInt(tbBaseF)<2 || parseInt(tbBaseF)>36 || isNaN(parseInt(tbBaseO)) || isNaN(parseInt(tbBaseF))) {
        alert("Alguna de las bases introducidas es menor de 2 o mayor de 36 o no es numérica.");
        return
    }
    
    if (tbValor!=parseInt(tbValor,tbBaseO).toString(tbBaseO)){ // comprobacion de que la base es la adecuada convirtiendo el valor dado a base 10 y reconvirtiendolo a base introducida como correcta
        alert("El valor no pertenece a la base introducida.");
    } else {
        document.getElementById('resultado').value = parseInt(tbValor, tbBaseO).toString(tbBaseF); // El parseInt acepta de 2 a 36.
    }  
}

window.onload = function() {
    var clickBoton = document.getElementById('boton');
    clickBoton.addEventListener("click", boton, false);
}