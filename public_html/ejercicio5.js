var pulsado;
var operaciones; // Definimos una segunda caja para poder ver que operaciones se están llevando a cabo.
var operador="";
var valorAcumulado = 0;
var coma = false;

function numero(valor) {
    pulsado.value += valor;
}

function operar(operando) {
    if (pulsado.value!="") { // controla que la caja no esté vacia y operar sobre nada
        if (operador=="" || operando=="," || operando=="=" || operando=="CE") { // control sobre operaciones a medias, menos en el caso de cambio de signo, igual o borrar donde es necesario que entre
            switch(operando){
                case "*":
                    operador = operando;
                    valorAcumulado = +pulsado.value;
                    operaciones.value+= pulsado.value + "*";
                    pulsado.value = "";
                    coma = false;
                    break;
                case "+":
                    operador = operando;
                    valorAcumulado = +pulsado.value;
                    operaciones.value+= pulsado.value + "+";
                    pulsado.value = "";
                    coma = false;
                    break;
                case "/":
                    operador = operando;
                    valorAcumulado = +pulsado.value;
                    operaciones.value+= pulsado.value + "/";
                    pulsado.value = "";
                    coma = false;
                    break;
                case "-":
                    operador = operando;
                    valorAcumulado = +pulsado.value;
                    operaciones.value+= pulsado.value + "-";
                    pulsado.value = "";
                    coma = false;
                    break;
                case "^":
                    operador = operando;
                    valorAcumulado = +pulsado.value;
                    operaciones.value+= pulsado.value + "^";
                    pulsado.value = "";
                    coma = false;
                    break;
                case "+/-":
                    pulsado.value= parseInt(pulsado.value)*(-1).toString(); // buena praxis: siempre devolver strings a cajas
                    coma = false;
                    break;
                case "√":
                    operador = operando;
                    operaciones.value+= "√"+pulsado.value;
                    if (isNaN(Math.sqrt(+pulsado.value,2))){ // control sobre si el numero es negativo usando isNaN
                        alert("Las raices no pueden operarse sobre valores negativos");
                    } else {
                        pulsado.value=Math.sqrt(+pulsado.value,2);
                    }
                    coma = false;
                    break;
                case ",":
                    if (!coma) { // control sobre la coma, no puede repetirse dentro de una parte de la operacion
                        pulsado.value=pulsado.value+"."; // se sustituye por un ., que es lo que entiende el math
                    } else {
                        alert("No se puede introducir más de una coma");
                    }
                    coma = true;
                    break;
                case "=":
                    resultado();
                    break;
                case "CE":
                    pulsado.value = "";
                    operaciones.value="";
                    coma = false;
                    operador="";
                    valorAcumulado = 0;
                    break;
            }
        } else {
            alert("Operación en curso. Pulse CE o continúe la operación");
        }
    } else {
        alert("Primero debe pulsar un número sobre el que operar.");
    }
    
}

function resultado() {
    switch(operador) {
        case "*":
            operaciones.value+= pulsado.value;
            pulsado.value = valorAcumulado*+pulsado.value;
            break;
        case "+":
            operaciones.value+= pulsado.value;
            pulsado.value = valorAcumulado + +pulsado.value;
            break;
        case "/":
            operaciones.value+= pulsado.value;
            if (isFinite(valorAcumulado/+pulsado.value)){ // control del divided by zero usando la funcion nativa isfinite
                pulsado.value = valorAcumulado/+pulsado.value;
            } else {
                alert("Indeterminación: división entre cero no permitida");
                pulsado.value = "Indeterminación";                
            }
            break; 
        case "-":
            operaciones.value+= pulsado.value;
            pulsado.value = valorAcumulado - +pulsado.value;
            break; 
        case "^":
            operaciones.value+= pulsado.value;
            pulsado.value = Math.pow(valorAcumulado,+pulsado.value);
            break;  
    }
}

window.onload = function() {
    pulsado = document.getElementById("caja");
    operaciones = document.getElementById("operaciones");
    for (var i=0; i<10; i++) { // aprovechandonos de la concatenacion de strings, creamos todos los liseners mediante un for colocando ids adecuadas en el html.
        document.getElementById('n'+i).addEventListener("click", function(){numero(this.value)}, false);
        document.getElementById('o'+i).addEventListener("click", function(){operar(this.value)}, false);
    }
}