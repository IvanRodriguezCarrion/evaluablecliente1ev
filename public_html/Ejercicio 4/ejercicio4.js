/*
Nota: Soy rolero y me parecio interesante adaptar este ejercicio a dados de 10 caras (Que es con lo que solemos jugar en este ambito). No obstante, primero hice el ejercicio con d6 para comprobar que funcionaba y lo adapte a mi manera.
*/

var caraDado = new Array(0,0,0,0,0,0,0,0,0,0);
var contador = 0;
var valorMax = 0;
var valorAct = 0;
var tiradas = function() {
    if(contador>20){ //controlamos el tiempo que va a estar actuando la funcion
         clearInterval(inter);
            for(var i=0; i<caraDado.length; i++) {
                if (caraDado[i] >= valorMax) { // comprobamos cual es el valor maximo, guardando su posicion en el vector. devuelve el valor máximo de la cara más alta (en caso de empate, la cara más alta)
                    valorAct = i;
                    valorMax = caraDado[i];
                }
            }
            alert('EL NÚMERO MAYOR MÁS REPETIDO ES EL ' + valorAct); // no hay manera de comprobar si dos sacan el mismo valor, saca el valor más alto en la cara del dado.
                    return;
    }
    var caraAct = Math.floor((Math.random() * 10)); // en el caso de ser un dado de seis la operación seria Math.random() * 6+1) porque querríamos devolver valores entre el 1 y el 6, ya que el random devuelve solo de 0 a 1.
    caraDado[caraAct]++; // incrementamos el valor de la posición del vector donde ha salido la cara
    document.getElementById('d'+caraAct).value = caraDado[caraAct]; // llenamos cada caja dinamicamente con el valor de su vector correspondiente
    document.getElementById('cara').setAttribute('src', 'd'+caraAct+'.png'); // recuperamos el valor de la imagen segun la cara actual
    contador++; // contador del tiempo
}	
var inter = setInterval(tiradas, 1000);


window.onload = function() {
    var clickBoton = document.getElementById('boton');
    clickBoton.addEventListener("click", tiradas, false);
}
