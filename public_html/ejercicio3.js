function boton () {
    
    var nombre = document.getElementById('nombre').value;
    var peso = document.getElementById('peso').value;
    var altura = document.getElementById('altura').value;
    var tabla = document.getElementById('listado');
    //creacion elementos
    var fila = document.createElement("tr");
    var celdNombre = document.createElement("td");
    var celdPeso = document.createElement("td");
    var celdAltura = document.createElement("td");
    
    var CNombre = document.createTextNode(nombre);
    var CPeso = document.createTextNode(peso);
    var CAltura = document.createTextNode(altura);
    
    //adicion de los elementos a sus padres
    celdNombre.appendChild(CNombre);
    celdPeso.appendChild(CPeso);
    celdAltura.appendChild(CAltura);
    
    fila.appendChild(celdNombre);
    fila.appendChild(celdPeso);
    fila.appendChild(celdAltura);
    
    tabla.appendChild(fila);
    
    //comprobacion exigida
    if (parseInt(peso) > (parseInt(altura)-100)) {
        celdNombre.setAttribute("style", "color:#d21318");
    } else{
        celdNombre.setAttribute("style", "color:#8c8cff");
    }
    
    limpiarCampos();
    
}

function limpiarCampos() {
    document.getElementById('nombre').value="";
    document.getElementById('peso').value="";
    document.getElementById('altura').value="";
}


window.onload = function() {
    var clickBoton = document.getElementById('boton');
    clickBoton.addEventListener("click", boton, false);
}